package main

import (
	"flag"
	"fmt"
)

func main() {
	operation := flag.String("operation", "start-socket", "operation - process-matches/start-socket")

	flag.Parse()
	fmt.Println("operation:", *operation)

	switch *operation {
	case "start-socket":
		StartSocket()
		break
	case "process-matches":
		ProcessMatches()
		break
	default:
		StartSocket()
		break
	}
}

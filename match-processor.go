package main

import (
	"context"
	"fmt"
	"log"

	"go.mongodb.org/mongo-driver/bson"
	"heliozen.com/multiplayer-api-socket/app"
	"heliozen.com/multiplayer-api-socket/config"
)

//ProcessMatches processes outstanding matches for ranking and stats
func ProcessMatches() {
	fmt.Printf("processing outstanding matches")
	configuration := config.GetConfiguration()
	client, _, cancel, err := configuration.Database.GetMongoClient()
	if err != nil {
		log.Printf(err.Error())
		return
	}

	defer cancel()
	database := client.Database(configuration.Database.DatabaseName)

	collection := database.Collection(configuration.Database.CollectionNames.Matches)
	ctx := context.Background()
	//get a list of all matches
	cursor, err := collection.Find(ctx, bson.M{})
	if err != nil {
		log.Fatal(err)
	}
	var matches []app.Match
	if err = cursor.All(ctx, &matches); err != nil {
		log.Fatal(err)
	}
	// fmt.Println(matches)
}

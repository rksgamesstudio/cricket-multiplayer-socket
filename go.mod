module heliozen.com/multiplayer-api-socket

go 1.14

require (
	github.com/gorilla/websocket v1.4.2
	go.mongodb.org/mongo-driver v1.3.2
	github.com/spf13/viper v1.6.3
)

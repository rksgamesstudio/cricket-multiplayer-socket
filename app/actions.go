package app

import (
	"encoding/json"
	"log"
)

//ProcessMessage processes a message
func (c *Client) ProcessMessage(message []byte) {
	// Declared an empty interface
	var jsonData interface{}
	// log.Println(string(message[:]))
	// Unmarshal or Decode the JSON to the interface.
	if jsonErr := json.Unmarshal(message, &jsonData); jsonErr != nil {
		log.Println("JSON parsing failed " + string(message))
		log.Println(jsonErr.Error())
	} else {
		request := jsonData.(map[string]interface{})
		if request["action"] == nil {
			return
		}
		// fmt.Println(fmt.Sprintf("%#v", request))
		log.Printf(request["action"].(string))
		switch request["action"] {
		case "login":
			go c.Login(request)
			break
		case "create-new-profile":
			go c.CreateNewProfile()
			break
		case "logout":
			go c.Logout(request)
			break
		case "matchmaking":
			go c.EnqueMatchRequest(request)
			break
		case "abort-matchmaking":
			go c.hub.AbortMatchRequest(request)
			break
		case "add-friend":
			go c.AddFriend(request)
			break
		case "create-friendrequest":
			go c.RequestFriendship(request)
			break

		case "confirm-friendrequest":
			go c.ConfirmFriendshipRequest(request)
			break

		case "decline-friendrequest":
			go c.DeclineFriendshipRequest(request)
			break

		case "get-friendlist":
			go c.SendAllFriends()
			break

		case "register":
			//registers particepents for a match
			go c.RegisterParticipent(request)
			break
		case "setParticipentAsServer":
			//register a participent as server
			go c.hub.matches[c.match.ID].SetParticipentAsServer(c)
			break
		case "unsetParticipentAsServer":
			go c.hub.matches[c.match.ID].UnsetParticipentAsServer(c)
			break
		case "setParticipentAsClient":
			//register a participent as server
			go c.hub.matches[c.match.ID].SetParticipentAsClient(c)
			break
		case "unsetParticipentAsClient":
			go c.hub.matches[c.match.ID].UnsetParticipentAsClient(c)
			break
		case "transportToServer":
			go c.hub.matches[c.match.ID].BroadcastToServer(message)
			break
		case "transportToClients":
			go c.hub.matches[c.match.ID].BroadcastToClients(request["connectionIds"].([]interface{}), message)
			break
		case "transport":
			if c.isServer {
				go c.hub.matches[c.match.ID].BroadcastToClients(request["connectionIds"].([]interface{}), message)
			} else {
				go c.hub.matches[c.match.ID].BroadcastToServer(message)
			}
			break

		case "broadcastMessage":
			go c.hub.matches[c.match.ID].SendMessageToParticipents(request, c)
			break
		default:
			break
		}
	}
}

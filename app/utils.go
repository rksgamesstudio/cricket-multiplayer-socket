package app

import (
	"context"
	"log"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

//GetProfileByUserID returns a profile coresponding to a given user id
func (h *Hub) GetProfileByUserID(userID primitive.ObjectID) *Profile {
	client, _, cancel, err := h.Configuration.Database.GetMongoClient()
	if err != nil {
		log.Printf(err.Error())
		return nil
	}
	defer cancel()
	//Insert matchmaking requst
	collection := client.Database(h.Configuration.Database.DatabaseName).Collection(h.Configuration.Database.CollectionNames.Profiles)

	var profile Profile
	filter := bson.M{"_id": userID}
	//res, err := collection.InsertOne(context.Background(), result)
	//err = collection.FindOne(ctx, bson.M{"deviceIdentifier": identifier}).Decode(&result)
	err = collection.FindOne(context.Background(), filter).Decode(&profile)

	if err != nil {
		log.Printf(err.Error())
		return nil
	}

	return &profile
}

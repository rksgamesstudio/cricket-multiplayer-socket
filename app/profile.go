// Copyright 2013 The Gorilla WebSocket Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package app

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"strconv"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

//CreateNewProfile Creates and sends a new profile
func (c *Client) CreateNewProfile() {
	client, _, cancel, err := c.hub.Configuration.Database.GetMongoClient()
	if err != nil {
		log.Printf(err.Error())

		return
	}
	defer cancel()

	database := client.Database(c.hub.Configuration.Database.DatabaseName)

	collection := database.Collection(c.hub.Configuration.Database.CollectionNames.Profiles)

	profile := Profile{
		Name:   "User" + strconv.Itoa(10000000+rand.Intn(99999999-10000000)),
		Level:  0,
		Points: 0,
	}

	res, err := collection.InsertOne(context.Background(), profile)
	profile.ID = res.InsertedID.(primitive.ObjectID)
	fmt.Println(fmt.Sprintf("%#v", res))
	response := make(map[string]interface{})
	response["resource"] = "profile"
	response["payload"] = profile
	responseByte, _ := json.Marshal(response)
	c.send <- responseByte
}

//NotifySuccessfulLogin Informs client about a successful login
func (c *Client) NotifySuccessfulLogin() {
	response := make(map[string]interface{})
	response["resource"] = "login-sucessful"
	responseByte, _ := json.Marshal(response)
	c.send <- responseByte
}

//Login Mark a client as logged in
func (c *Client) Login(request map[string]interface{}) {
	//add a record for client and device association
	client, _, cancel, err := c.hub.Configuration.Database.GetMongoClient()
	if err != nil {
		log.Printf(err.Error())

		return
	}
	defer cancel()

	database := client.Database(c.hub.Configuration.Database.DatabaseName)

	collection := database.Collection(c.hub.Configuration.Database.CollectionNames.UserDevices)
	id, err := primitive.ObjectIDFromHex(request["user"].(string))
	if err != nil {
		return
	}
	userDevice := UserDevice{
		User:     id,
		DeviceID: request["identifier"].(string),
		Online:   true,
		Time:     primitive.Timestamp{T: uint32(time.Now().Unix())},
	}

	_, err = collection.InsertOne(context.Background(), userDevice)

	if err != nil {
		return
	}

	//assign id to client and register the client on hub
	c.ID = id
	c.DeviceID = userDevice.DeviceID
	//register client on hub
	c.hub.register <- c
}

//Logout Mark a client as logged out
func (c *Client) Logout(request map[string]interface{}) {
	//add a record for client and device association
	client, _, cancel, err := c.hub.Configuration.Database.GetMongoClient()
	if err != nil {
		log.Printf(err.Error())

		return
	}
	defer cancel()

	userID, err := primitive.ObjectIDFromHex(request["user"].(string))
	if err != nil {
		return
	}
	database := client.Database(c.hub.Configuration.Database.DatabaseName)

	collection := database.Collection(c.hub.Configuration.Database.CollectionNames.UserDevices)

	var result UserDevice
	err = collection.FindOne(context.Background(), bson.M{"deviceId": request["identifier"].(string), "user": userID}).Decode(&result)

	if err != nil {
		return
	}

	filter := bson.M{"_id": bson.M{"$eq": result.ID}}
	update := bson.M{"$set": bson.M{"online": false}}
	_, err = collection.UpdateOne(
		context.Background(),
		filter,
		update,
	)
	fmt.Println("Done updating")

	c.hub.unregister <- c

}

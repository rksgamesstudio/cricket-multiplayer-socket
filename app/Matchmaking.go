package app

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"strconv"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

//EnqueMatchRequest handles a matchmakingrequest
func (c *Client) EnqueMatchRequest(request map[string]interface{}) {
	client, _, cancel, err := c.hub.Configuration.Database.GetMongoClient()
	if err != nil {
		log.Printf(err.Error())
		return
	}
	defer cancel()
	//Insert matchmaking requst
	collection := client.Database(c.hub.Configuration.Database.DatabaseName).Collection(c.hub.Configuration.Database.CollectionNames.MatchRequests)

	currentRequest := MatchRequest{
		DeviceID: c.DeviceID,
		User:     c.ID,
		IsActive: true,
		Stadium:  int(request["stadium"].(float64)),
		Time:     primitive.Timestamp{T: uint32(time.Now().Unix())},
	}

	teamByte, err := json.Marshal(request["team"].(interface{}))
	if err == nil {
		currentRequest.Team = string(teamByte)
	}

	teamPlayersByte, err := json.Marshal(request["players"].(interface{}))
	if err == nil {
		currentRequest.Players = string(teamPlayersByte)
	}

	res, err := collection.InsertOne(context.Background(), currentRequest)
	if err != nil {
		log.Printf(err.Error())
		return
	}
	currentRequest.ID = res.InsertedID.(primitive.ObjectID)
	response := make(map[string]interface{})
	response["resource"] = "match-request-created"
	response["payload"] = currentRequest
	responseByte, err := json.Marshal(response)
	if err != nil {
		log.Printf(err.Error())
		return
	}

	c.send <- responseByte
	//perform the matchmaking
	//try to find a request for same stadium
	var otherRequest MatchRequest
	filter := bson.M{"isActive": true, "stadium": int(request["stadium"].(float64)), "deviceId": bson.M{"$ne": c.DeviceID}}
	//res, err := collection.InsertOne(context.Background(), result)
	//err = collection.FindOne(ctx, bson.M{"deviceIdentifier": identifier}).Decode(&result)
	err = collection.FindOne(context.Background(), filter).Decode(&otherRequest)

	if err == nil {
		//we have a request with same stadium
		//check if requested client is still online

		if _, ok := c.hub.clients[otherRequest.DeviceID]; !ok {
			abortRequest := make(map[string]interface{})
			abortRequest["matchRequestID"] = otherRequest.ID.Hex()
			c.hub.AbortMatchRequest(abortRequest)
			return
		}

		filter = bson.M{"_id": bson.M{"$eq": currentRequest.ID}}
		update := bson.M{"$set": bson.M{"isActive": false}}
		collection.UpdateOne(
			context.Background(),
			filter,
			update,
		)

		filter = bson.M{"_id": bson.M{"$eq": otherRequest.ID}}
		update = bson.M{"$set": bson.M{"isActive": false}}
		collection.UpdateOne(
			context.Background(),
			filter,
			update,
		)

		//both clients are online
		matchDetails := &Match{
			AwayDeviceID:      currentRequest.DeviceID,
			AwayPlayerProfile: *c.hub.GetProfileByUserID(currentRequest.User),
			HomeDeviceID:      otherRequest.DeviceID,
			HomePlayerProfile: *c.hub.GetProfileByUserID(otherRequest.User),
			Stadium:           currentRequest.Stadium,
			HomeTeam:          otherRequest.Team,
			HomeTeamPlayers:   otherRequest.Players,
			AwayTeam:          currentRequest.Team,
			AwayTeamPlayers:   currentRequest.Players,
		}

		matchCollection := client.Database(c.hub.Configuration.Database.DatabaseName).Collection(c.hub.Configuration.Database.CollectionNames.Matches)

		res, err := matchCollection.InsertOne(context.Background(), matchDetails)
		matchDetails.ID = res.InsertedID.(primitive.ObjectID)
		matchPayload := make(map[string]interface{})
		matchPayload["awayDeviceID"] = currentRequest.DeviceID
		matchPayload["awayPlayerProfile"] = matchDetails.AwayPlayerProfile
		matchPayload["homeDeviceID"] = otherRequest.DeviceID
		matchPayload["homePlayerProfile"] = matchDetails.HomePlayerProfile
		matchPayload["stadium"] = matchDetails.Stadium
		matchPayload["id"] = matchDetails.ID.Hex()
		log.Printf("sending match payload with ID " + matchDetails.ID.Hex())
		var homeTeam interface{}
		// Unmarshal or Decode the JSON to the interface.
		if jsonErr := json.Unmarshal([]byte(matchDetails.HomeTeam), &homeTeam); jsonErr == nil {
			matchPayload["homeTeam"] = homeTeam
		}

		var homeTeamPlayers interface{}
		// Unmarshal or Decode the JSON to the interface.
		if jsonErr := json.Unmarshal([]byte(matchDetails.HomeTeamPlayers), &homeTeamPlayers); jsonErr == nil {
			matchPayload["homeTeamPlayers"] = homeTeamPlayers
		}

		var awayTeam interface{}
		// Unmarshal or Decode the JSON to the interface.
		if jsonErr := json.Unmarshal([]byte(matchDetails.AwayTeam), &awayTeam); jsonErr == nil {
			matchPayload["awayTeam"] = awayTeam
		}

		var awayTeamPlayers interface{}
		// Unmarshal or Decode the JSON to the interface.
		if jsonErr := json.Unmarshal([]byte(matchDetails.AwayTeamPlayers), &awayTeamPlayers); jsonErr == nil {
			matchPayload["awayTeamPlayers"] = awayTeamPlayers
		}

		response := make(map[string]interface{})
		response["resource"] = "match-created"
		response["payload"] = matchPayload
		responseByte, err := json.Marshal(response)
		if err != nil {
			log.Printf(err.Error())
			return
		}
		log.Printf("sending request to clients " + strconv.Itoa((len(c.hub.clientDevices[currentRequest.User]))))
		//send data to both clients
		if _, ok := c.hub.clients[currentRequest.DeviceID]; ok {
			log.Printf("found current request device id")
			c.hub.clients[currentRequest.DeviceID].send <- responseByte
		}

		if _, ok := c.hub.clients[otherRequest.DeviceID]; ok {
			log.Printf("found other request device id")
			c.hub.clients[otherRequest.DeviceID].send <- responseByte
		}
	}

}

//AbortMatchRequest Try to perfom Mathmaking
func (h *Hub) AbortMatchRequest(request map[string]interface{}) {
	client, _, cancel, err := h.Configuration.Database.GetMongoClient()
	if err != nil {
		log.Printf(err.Error())
		return
	}
	defer cancel()
	//Insert matchmaking requst
	collection := client.Database(h.Configuration.Database.DatabaseName).Collection(h.Configuration.Database.CollectionNames.MatchRequests)
	var matchRequest MatchRequest

	requestID, err := primitive.ObjectIDFromHex(request["matchRequestID"].(string))
	if err != nil {
		log.Printf(err.Error())
		return
	}

	filter := bson.M{"_id": requestID}
	//res, err := collection.InsertOne(context.Background(), result)
	//err = collection.FindOne(ctx, bson.M{"deviceIdentifier": identifier}).Decode(&result)
	err = collection.FindOne(context.Background(), filter).Decode(&matchRequest)

	if err != nil {
		log.Printf(err.Error())
		return
	}
	filter = bson.M{"_id": bson.M{"$eq": matchRequest.ID}}
	update := bson.M{"$set": bson.M{"isActive": false}}
	collection.UpdateOne(
		context.Background(),
		filter,
		update,
	)
	fmt.Println("Done updating")

	matchRequest.IsActive = false
	response := make(map[string]interface{})
	response["resource"] = "match-request-aborted"
	response["payload"] = matchRequest
	responseByte, err := json.Marshal(response)
	if err != nil {
		log.Printf(err.Error())
		return
	}
	//send data to both clients
	if _, ok := h.clients[matchRequest.DeviceID]; ok {
		h.clients[matchRequest.DeviceID].send <- responseByte
		return
	}

}

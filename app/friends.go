package app

import (
	"context"
	"encoding/json"
	"fmt"
	"log"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

//AddFriend Create a friend request
func (c *Client) AddFriend(request map[string]interface{}) {
	client, _, cancel, err := c.hub.Configuration.Database.GetMongoClient()
	if err != nil {
		log.Printf(err.Error())
		return
	}
	defer cancel()

	database := client.Database(c.hub.Configuration.Database.DatabaseName)

	collection := database.Collection(c.hub.Configuration.Database.CollectionNames.Friendships)
	toID, err := primitive.ObjectIDFromHex(request["to"].(string))
	if err != nil {
		log.Printf(err.Error())
		return
	}
	friendship := Friendship{
		To:        toID,
		From:      c.ID,
		Confirmed: true,
		Pending:   false,
	}

	res, err := collection.InsertOne(context.Background(), friendship)
	friendship.ID = res.InsertedID.(primitive.ObjectID)
	fmt.Println(fmt.Sprintf("%#v", res))
	response := make(map[string]interface{})
	response["resource"] = "friend-request"
	response["payload"] = friendship
	response["user"] = c.hub.GetProfileByUserID(friendship.From)
	friendshipResp, _ := json.Marshal(response)
	for _, deviceID := range c.hub.clientDevices[toID] {
		if _, ok := c.hub.clients[deviceID]; ok {
			c.hub.clients[deviceID].send <- friendshipResp
		}
	}

}

//RequestFriendship Create a friend request
func (c *Client) RequestFriendship(request map[string]interface{}) {
	client, _, cancel, err := c.hub.Configuration.Database.GetMongoClient()
	if err != nil {
		log.Printf(err.Error())
		return
	}
	defer cancel()

	database := client.Database(c.hub.Configuration.Database.DatabaseName)

	collection := database.Collection(c.hub.Configuration.Database.CollectionNames.Friendships)
	toID, err := primitive.ObjectIDFromHex(request["to"].(string))
	if err != nil {
		log.Printf(err.Error())
		return
	}
	friendship := Friendship{
		To:        toID,
		From:      c.ID,
		Confirmed: false,
		Pending:   true,
	}

	res, err := collection.InsertOne(context.Background(), friendship)
	friendship.ID = res.InsertedID.(primitive.ObjectID)
	fmt.Println(fmt.Sprintf("%#v", res))
	response := make(map[string]interface{})
	response["resource"] = "friend-request"
	response["payload"] = friendship
	response["user"] = c.hub.GetProfileByUserID(friendship.From)
	friendshipResp, _ := json.Marshal(response)
	for _, deviceID := range c.hub.clientDevices[toID] {
		if _, ok := c.hub.clients[deviceID]; ok {
			c.hub.clients[deviceID].send <- friendshipResp
		}
	}

}

//ConfirmFriendshipRequest confirm friendship request with the target player
func (c *Client) ConfirmFriendshipRequest(request map[string]interface{}) {
	//add a record for client and device association
	client, _, cancel, err := c.hub.Configuration.Database.GetMongoClient()
	if err != nil {
		log.Printf(err.Error())

		return
	}
	defer cancel()

	friendShipID, err := primitive.ObjectIDFromHex(request["friendrequestID"].(string))
	if err != nil {
		return
	}
	database := client.Database(c.hub.Configuration.Database.DatabaseName)

	collection := database.Collection(c.hub.Configuration.Database.CollectionNames.Friendships)

	filter := bson.M{"_id": bson.M{"$eq": friendShipID}}
	update := bson.M{"$set": bson.M{"confirmed": true, "pending": false}}
	_, err = collection.UpdateOne(
		context.Background(),
		filter,
		update,
	)
	fmt.Println("Done updating")

	//notify the from friendship user
	var friendship Friendship
	err = collection.FindOne(context.Background(), bson.M{"_id": friendShipID}).Decode(&friendship)

	if err != nil {
		log.Println(err.Error())
		return
	}

	response := make(map[string]interface{})
	response["resource"] = "friend-confirmed"
	response["payload"] = friendship
	response["user"] = c.hub.GetProfileByUserID(friendship.To)
	friendshipResp, _ := json.Marshal(response)
	for _, deviceID := range c.hub.clientDevices[friendship.From] {
		if _, ok := c.hub.clients[deviceID]; ok {
			c.hub.clients[deviceID].send <- friendshipResp
		}
	}

}

//DeclineFriendshipRequest confirm friendship request with the target player
func (c *Client) DeclineFriendshipRequest(request map[string]interface{}) {
	//add a record for client and device association
	client, _, cancel, err := c.hub.Configuration.Database.GetMongoClient()
	if err != nil {
		log.Printf(err.Error())

		return
	}
	defer cancel()

	friendShipID, err := primitive.ObjectIDFromHex(request["friendrequestID"].(string))
	if err != nil {
		return
	}
	database := client.Database(c.hub.Configuration.Database.DatabaseName)

	collection := database.Collection(c.hub.Configuration.Database.CollectionNames.Friendships)

	filter := bson.M{"_id": bson.M{"$eq": friendShipID}}
	update := bson.M{"$set": bson.M{"confirmed": false, "pending": false}}
	_, err = collection.UpdateOne(
		context.Background(),
		filter,
		update,
	)
	fmt.Println("Done updating")

	//notify the from friendship user
	var friendship Friendship
	err = collection.FindOne(context.Background(), bson.M{"_id": friendShipID}).Decode(&friendship)

	if err != nil {
		log.Println(err.Error())
		return
	}

	response := make(map[string]interface{})
	response["resource"] = "friend-denied"
	response["payload"] = friendship
	response["user"] = c.hub.GetProfileByUserID(friendship.To)
	friendshipResp, _ := json.Marshal(response)
	for _, deviceID := range c.hub.clientDevices[friendship.From] {
		if _, ok := c.hub.clients[deviceID]; ok {
			c.hub.clients[deviceID].send <- friendshipResp
		}
	}
}

//SendPendingFriendships List all pending friendship requests
func (c *Client) SendPendingFriendships() {

	friendships := c.GetAllFriends()

	response, err := json.Marshal(friendships)

	if err != nil {
		return
	}

	c.send <- response

}

//SendAllFriends List all friendships
func (c *Client) SendAllFriends() {
	client, _, cancel, err := c.hub.Configuration.Database.GetMongoClient()
	if err != nil {
		log.Printf(err.Error())

		return
	}
	defer cancel()

	database := client.Database(c.hub.Configuration.Database.DatabaseName)

	collection := database.Collection(c.hub.Configuration.Database.CollectionNames.Friendships)

	matchStage := bson.D{{Key: "$match", Value: bson.M{
		"confirmed": true,
		"$or": []interface{}{
			bson.M{"to": c.ID},
			bson.M{"from": c.ID},
		},
	}}}
	toUserLookupStage := bson.D{{Key: "$lookup", Value: bson.D{
		{Key: "from", Value: c.hub.Configuration.Database.CollectionNames.Profiles},
		{Key: "localField", Value: "to"},
		{Key: "foreignField", Value: "_id"},
		{Key: "as", Value: "toUser"}},
	}}

	fromUserLookupStage := bson.D{{Key: "$lookup", Value: bson.D{
		{Key: "from", Value: c.hub.Configuration.Database.CollectionNames.Profiles},
		{Key: "localField", Value: "from"},
		{Key: "foreignField", Value: "_id"},
		{Key: "as", Value: "fromUser"}},
	}}

	toUserUnwindStage := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$toUser"}, {Key: "preserveNullAndEmptyArrays", Value: false}}}}
	fromUserUnwindStage := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$fromUser"}, {Key: "preserveNullAndEmptyArrays", Value: false}}}}

	friendListCursor, err := collection.Aggregate(context.Background(), mongo.Pipeline{matchStage, toUserLookupStage, toUserUnwindStage, fromUserLookupStage, fromUserUnwindStage})
	if err != nil {
		log.Printf(err.Error())
		return
	}

	var friendships []bson.M
	if err = friendListCursor.All(context.Background(), &friendships); err != nil {
		log.Printf(err.Error())
		return
	}
	var friendID primitive.ObjectID
	for _, friendship := range friendships {
		friendship["online"] = false
		if friendship["to"] == c.ID {
			friendID = friendship["from"].(primitive.ObjectID)
		} else {
			friendID = friendship["to"].(primitive.ObjectID)
		}
		for _, deviceID := range c.hub.clientDevices[friendID] {
			if _, ok := c.hub.clients[deviceID]; ok {
				friendship["online"] = true
			}
		}
	}

	response := make(map[string]interface{})
	response["resource"] = "friendlist"
	response["payload"] = friendships

	responseByte, err := json.Marshal(response)

	if err != nil {
		return
	}

	c.send <- responseByte

}

//GetAllFriends returns all confirmed friendships
func (c *Client) GetAllFriends() []Friendship {
	//add a record for client and device association
	client, _, cancel, err := c.hub.Configuration.Database.GetMongoClient()
	if err != nil {
		log.Printf(err.Error())

		return nil
	}
	defer cancel()

	database := client.Database(c.hub.Configuration.Database.DatabaseName)

	collection := database.Collection(c.hub.Configuration.Database.CollectionNames.Friendships)

	var friendships []Friendship

	filter := bson.M{
		"confirmed": true,
		"$or": []interface{}{
			bson.M{"to": c.ID},
			bson.M{"from": c.ID},
		},
	}
	results, err := collection.Find(context.Background(), filter)

	if err != nil {
		log.Printf(err.Error())
		return nil
	}

	if err = results.All(context.Background(), &friendships); err != nil {
		log.Fatal(err)
	}

	fmt.Println(fmt.Sprintf("%#v", friendships))

	return friendships
}

//NotifyOnlineFriends notifies online friends
func (h *Hub) NotifyOnlineFriends(c *Client) {
	friendships := c.GetAllFriends()
	response := make(map[string]interface{})
	response["action"] = "friend-online-event"
	response["user"] = h.GetProfileByUserID(c.ID)

	respByte, err := json.Marshal(response)
	if err != nil {
		return
	}

	//list of all confirmed friend id
	var friendID primitive.ObjectID
	for _, friendship := range friendships {
		friendID = friendship.GetFriendID(c)
		for _, deviceID := range h.clientDevices[friendID] {
			if _, ok := h.clients[deviceID]; ok {
				h.clients[deviceID].send <- respByte
			}
		}
	}

}

//GetFriendID returns friend id based on client id
func (f *Friendship) GetFriendID(c *Client) primitive.ObjectID {
	if f.To == c.ID {
		return f.From
	}
	return f.To
}

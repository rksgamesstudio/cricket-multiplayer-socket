// Copyright 2013 The Gorilla WebSocket Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package app

import (
	"log"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"heliozen.com/multiplayer-api-socket/config"
)

// Hub maintains the set of active clients and broadcasts messages to the
// clients.
type Hub struct {
	// Registered clients.
	clients map[string]Client

	//client devies
	clientDevices map[primitive.ObjectID][]string

	//Registered matches
	matches map[primitive.ObjectID]*MatchDetails

	// Inbound messages from the clients.
	broadcast chan []byte

	// Register requests from the clients.
	register chan *Client

	// Register requests from the clients.
	registerParticipent chan *Client

	// Unregister requests from clients.
	unregister chan *Client

	// Unregister requests from clients.
	unregisterParticipent chan *Client

	// Server Config
	Configuration config.Configuration
}

//NewHub Create and return an instance of hub
func NewHub(config config.Configuration) *Hub {
	return &Hub{
		broadcast:             make(chan []byte),
		register:              make(chan *Client),
		registerParticipent:   make(chan *Client),
		unregister:            make(chan *Client),
		unregisterParticipent: make(chan *Client),
		clients:               make(map[string]Client),
		clientDevices:         make(map[primitive.ObjectID][]string),
		Configuration:         config,
		matches:               make(map[primitive.ObjectID]*MatchDetails),
	}
}

//Run bootstrap hub
func (h *Hub) Run() {
	for {
		select {
		case client := <-h.register:
			h.clients[client.DeviceID] = *client

			if _, ok := h.clientDevices[client.ID]; !ok {
				log.Printf("declare new device lists")
				h.clientDevices[client.ID] = []string{}
			}
			h.clientDevices[client.ID] = append(h.clientDevices[client.ID], client.DeviceID)
			log.Printf("registering client with ID " + client.ID.Hex() + " with device id  " + client.DeviceID)
			client.NotifySuccessfulLogin()
			h.NotifyOnlineFriends(client)
		case client := <-h.registerParticipent:
			if _, ok := h.matches[client.match.ID]; !ok {
				h.matches[client.match.ID] = &MatchDetails{
					Match:              *client.match,
					Clients:            make(map[int]*Client),
					LastClientID:       0,
					Participants:       make(map[string]*Client),
					ParticipantDevices: make(map[primitive.ObjectID][]string),
				}
			}
			h.matches[client.match.ID].Participants[client.DeviceID] = client
			if _, ok := h.matches[client.match.ID].ParticipantDevices[client.ID]; !ok {
				log.Printf("declare new device lists")
				h.matches[client.match.ID].ParticipantDevices[client.ID] = []string{}
			}
			h.matches[client.match.ID].ParticipantDevices[client.ID] = append(h.matches[client.match.ID].ParticipantDevices[client.ID], client.DeviceID)
			client.isLoggedIn = true
			h.matches[client.match.ID].OnRegistrationComplete(client)
		case client := <-h.unregisterParticipent:
			if client.isLoggedIn {
				if _, ok := h.matches[client.match.ID]; ok {
					h.matches[client.match.ID].RemoveParticipent(client)
				}
				client.isLoggedIn = false
			}
		case client := <-h.unregister:
			if _, ok := h.clients[client.DeviceID]; ok {
				delete(h.clients, client.DeviceID)
				close(client.send)
			}
			//check if a client id exists
			if _, ok := h.clientDevices[client.ID]; ok {
				for i, deviceID := range h.clientDevices[client.ID] {
					if deviceID == client.DeviceID {
						h.clientDevices[client.ID] = append(h.clientDevices[client.ID][:i], h.clientDevices[client.ID][i+1:]...)
						break
					}
				}

				if len(h.clientDevices[client.ID]) == 0 {
					delete(h.clientDevices, client.ID)
				}
			}
		case message := <-h.broadcast:
			for ID, client := range h.clients {
				select {
				case client.send <- message:
				default:
					close(client.send)
					delete(h.clients, ID)
				}
			}
		}
	}
}

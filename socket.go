// Copyright 2013 The Gorilla WebSocket Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"

	"heliozen.com/multiplayer-api-socket/app"
	"heliozen.com/multiplayer-api-socket/config"
)

var addr = flag.String("addr", ":8080", "http service address")

//StartSocket block
func StartSocket() {

	flag.Parse()

	log.SetFlags(log.LstdFlags | log.Lshortfile)

	configuration := config.GetConfiguration()

	hub := app.NewHub(*configuration)
	go hub.Run()
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		app.ServeWs(hub, w, r)
	})
	fmt.Printf("port for this application is " + *addr + "\n")
	err := http.ListenAndServe(*addr, nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
